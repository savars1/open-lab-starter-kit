# Choosing a CNC Control Software

There are many different software solutions out there.Some are free or open source, while others are linked to a specific control board.

Before diving deeper into the pros and cons of each software, we need to understand how a CNC milling machine is controlled. The electronics that control a CNC are basically composed like this:- 

- Each axis is controlled by one or two motors ;
- Each motor gets its instructions from a driver ;
- A central breakout board sends instructions to the different drivers, based on the information it gets from a control software;
- A control software, which is directly connected to the controller with a transfer protocol (usually USB or Parallel DB25). Except on industrial CNC machines, the control software is usually running on an external device like a computer.

![Diagram showing control](https://wikifactory.com/files/RmlsZTo0ODQwMTc=)

What one needs to pay attention to when choosing a control software is:
- The intuitiveness of the user interface
- The hardware you're using: what type of controller + what type of device running the software
- The transfer protocol
- The documentation coming with the software + the community activity + the frequency of updates

## Mach3

**Mach3** is without any doubt the most popular software on the market so far.

![Mach3 frontend](https://wikifactory.com/files/RmlsZTo0ODQwMTk=)

Mach3 is a control software compatible with many hardware solutions and has positioned itself as the market leader for low-end CNC control software. Its success is mainly historical as it came in at a time when its interface was a lot more user-friendly than what existed on the market so far.

**Pros**

An easy-to-use User Interface that still does the job, even if its design looks like you're back in the '90s;
The interface itself is highly customizable to adapt it to your needs
As 70% of hobby CNC users uses Mach3, a lot of documentation is available online through its community of users.
Some custom features are very handy, like spindle control, video display or relay control.

**Cons**

Mach3 uses a parallel-port transfer protocol which isn't compatible with most computers today.
Note: Artsoft has since developed Mach4, which they claim to have begun from scratch and have solved almost all gaps of Mach3, but struggle to make their community switch from Mach3 to Mach4.

## LinuxCNC

The "father" of all low-end CNC software, created in 1993, is a free open source Linux software. Most further control software - like Mach3 - have based their original code on the LinuxCNC project.

![LinusCNC](https://wikifactory.com/files/RmlsZTo0ODQwMjA=)

The project benefits from a huge open source community that has developed several versions of the software and has contributed to the fact that it's still the second most used CNC control software today.

**Pros**

- As numerous people have worked on this software, it offers a huge flexibility. It may, however, be confusing for some beginning CNC milling or looking for a compact software without unnecessary options
- The community of LinuxCNC offers a lot of help to newcomers and is almost always able to solve issues. 

The features and possibilities of LinuxCNC are almost endless, but the learning curve is also rather steep.

**Cons**

- The software may seem overwhelming at first;
- As a real-time application, it runs on parallel transfer and doesn't support a USB-parallel adapter, which is hard to use on modern computers
- It's quite complicated to adapt the user interface to your needs.

## GRBL + USG

GRBL is - technically speaking - a CNC control software, but it doesn't come with a user interface. It is actually an open source software that allows microcontrollers like Arduino to receive G-code from a computer, through USB transfer.

![USG](https://wikifactory.com/files/RmlsZTo0ODQwMjE=)

It is usually combined with a G-code sender software that allows the user to transfer G-code to the Arduino. One software has been sailing through the multitude of available software : the Universal G-code Sender. 

**Pros**

- Arduino is a well-known microcontroller and it's quite easy for DIYers to build a homemade CNC with the GRBL system
- It's the easiest setup for small machines and first-time home made CNC's
- Software is free and hardware is cheap
- You can choose your favourite software to generate G-code.