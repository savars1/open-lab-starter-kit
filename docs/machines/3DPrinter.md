# Table of Contents

[[_TOC_]]

# Desktop 3D Printer

Preliminary research was carried out to see which open source designs and commercial 3D are popular in the 3D printer community. These will be used as a starting point for the development of the desktop 3D Printer. Various repositories such as Youtube, Github, Instructables, GrabCad, Thingiverse, Personal Blogs and Technical Forums were searched.

## Popular Open Source/Kit 3D Printers

1. The [Prusa i3MK3](https://www.prusa3d.com/) is one of the most popular open source 3D printers which consistently ranks among the [best printers](https://all3dp.com/1/best-3d-printer-reviews-top-3d-printers-home-3-d-printer-3d/) money can buy. The printer currently costs about 800. However since many parts have to still be bought from Prusa, it isn't an ideal choice for people living outside Europe or in developing countries.

<img src= "https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2019/03/15155153/prusa6.jpg" alt="prusa i3" width="500" height="400">

2. The [Vulcaman V1 Reprap 3D-Printer](https://www.instructables.com/Vulcanus-V1-3D-Printer/) with a build cost of 300€ is a [CoreXY](http://corexy.com/theory.html) fully DIY 3D printer based on the reprap model. It has a fully enclosed design and the design is licensed under Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
   - BOM, CAD and a detailed instruction manual are available.
   - Much interaction from community on the design and 15+ rebuilds which shows the design is relatively simple with good instructions and is reproducible.
   - The shape is an a boxed construction which lends to its rigidity and it is also enclosed.

<img src="https://content.instructables.com/ORIG/F7W/NLHN/I9IBO9VQ/F7WNLHNI9IBO9VQ.jpg?auto=webp&frame=1&height=1024&fit=bounds&md=bd7018083287778af70e2362185266ae" alt="Vulcaman V1 Reprap Model" width="450" height="450" style="vertical-align:middle">

    Specifications:
   - Dimensions: 44cm x 44cm x 60cm
   - Build Volume: 20cm x 20cm X 26cm
   - Travel Speed: 300mm/s
   - Resolution: up to 0.05mm
   - Electronic: Ramps 1.4 with TMC2100 1/256 microstep Motordriver

3. The [Falla 3D](http://www.falla3d.com/index_en.html) is an OpenSource 3D Printer with a magnetic levitation system for the bearings of the X and Y axes. The printer is also modular (it can extrude all 3mm and 1.75 mm filaments) and is scalable.
   - BOM and CAD files exist on [github](https://github.com/3dita/Falla3D)
  
<img src="http://www.falla3d.com/images/screenshots/45dx.jpg" width="350" height="350" style="vertical-align:middle">

 - Specifications:
   - Scalable up to a 90x60x60 cm printing area (Printer size 100x70x70 cm)
   - Double Extruder - With a single FUM hotend, with a classic double 3mm hotend or with a 1.75 Bowden system.

4. The [D-bot Core XY 3D printer](https://www.thingiverse.com/thing:1001065) from user Spauda01 on Makerbot's Thingiverse is another solid aluminium frame design with a coreXY mechanism. The design has high community interaction and over 140 replications. 
- BOM, CAD and Build instructins (video and pdf guide) available
- Costs about 550$

<img src="http://i.imgur.com/oFbBbEb.jpg" width="350" height="350" style="vertical-align:middle">

 - Build Volume: 300mm x 200mm x 325mm
 - Licences under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) i.e. commercial as well.

5.	[Voron v2.4](https://vorondesign.com/) by Paul Nokel is a high end DIY 3D Printer that is fully open source and is popular for having excellent print quality and speed along with intuitive assembly documentation. The Voron 2.1 uses mains current to heat the stationary bed, and 12v everywhere else. The printer is however on the expensive side of DIY prinitng and is realtively complicated for someone new in 3D printing.

<img src= "https://www.elektrifiziert.net/index.php?attachment/3257-2020-11-18-06h27-57-png/" alt="Voron 2.4" width="500" height="500">

- [User buildlog](https://www.elektrifiziert.net/forum/index.php?thread/134-voron-2-4-3d-drucker-selbstbau-tagebuch/)

6.	[Hypercube 3D Printer](https://www.thingiverse.com/thing:1752766) is a rigid boxed shape 3D printer made from 2020 aluminium extrusion profiles from user Tech2C and is very popular on the thingiverse website. The design also employs the coreXY mechanism with the attempt to reduce the X-carriage weight. The printer however has a cantilevered bed which causes Z-wobble in prints mainly noticed in printers that have tried to scale the design to a larger build volume.

8.	[Hypercube Evolution](https://www.thingiverse.com/thing:2254103) (also known as the HEVO), developed by SCOTT_3D, is an iteration or remix of the HyperCube 3D printer designed by Tech2C. It adds 3030 extrusions around the frame for added thickness, as well as a few more upgrades. It has a decent Wiki as well as a couple of different BOM generators, online and in Excel format. The design comes with configurable CAD files for required build volume and single or double Z axis motors. The build is relatively simple and allows modifications. The design is fully open source i.e. users can use the design, modify it and also use it for commercial purposes. The design is very popular in the community, since it improves on the previous design by tech2C.
6.	[Hypercube 3D Printer](https://www.thingiverse.com/thing:1752766) from User Tech2C
   - Clear BOM, CAD and Video build log

7.	[Hypercube Evolution](https://www.thingiverse.com/thing:2254103) (also known as the HEVO), developed by SCOTT_3D, is an iteration on the HyperCube 3D printer designed by Tech2C. It adds 3030 extrusions around the frame for added rigidity, as well as a few more upgrades. It has a decent Wiki as well as a couple of different BOM generators, online and in Excel format. The design comes with configurable CAD files for required build volume and single or double Z axis motors. 

![HEVO](https://i.pinimg.com/564x/60/33/9d/60339d8bd74040bed336274ebb4f9195.jpg)

- [HEVO build guide](https://bestin-it.com/how-to-build-best-3d-printer-from-scratch-hypercube-evolution/) from a german maker detailing his build and sourcing of parts
- [Fusion360 Model from user1](https://myhub.autodesk360.com/ue28de06e/g/shares/SH7f1edQT22b515c761edd5edf41a5b4aa05?viewState=NoIgbgDAdAjCA0IDeAdEAXAngBwKZoC40ARXAZwEsBzAOzXjQEMyzd1C0B2CAEwCY%2BjGADMAtAGYIAI1yiALOMZ9RATjkA2FfIiMe43Ll25xAYzQBfEAF0gA&fbclid=IwAR2G9gEP79uHXTaEnc1MUpbw_OhZn3VWZfzaTZHgxxt_8QGHX8jGW89UFPA)
- [Fuson360 Model from User2](https://myhub.autodesk360.com/ue28de06e/g/shares/SH7f1edQT22b515c761e627c356ab003cc8e?viewState=NoIgbgDAdAjCA0IDeAdEAXAngBwKZoC40ARXAZwEsBzAOzXjQEMyzd1C0B2CAEwCY%2BjGADMAtAGYIAI1yiALOMZ9RATjkA2FfIiMe43Ll25xAYzQBfEAF0gA)


8. [Hypercube Evolution Oliver RT or HevORT](https://miragec79.github.io/HevORT/) is a remix **high end** DIY 3D Printer based on the Hypercube Evolution design which can cost about 2000 USD. The printer has auto-levelling of bed using 3 extra z-axis motors. It has a rigid Gantry for achieving high accelerations and speed using a direct drive extruder, whereby the user has achieved a build speed of 500mm/s+ with the printer.
- [Github repository](https://github.com/MirageC79/HevORT) and website has printer configurator, BOM generator and fusion 360 CAD model.

<u> **Some Commercial and semi open Source (DIY) Desktop Printers** </u>

1. Stratasys (Industrial Higher End)
2. Ultimaker (Mid consumer however still expensive) - These are considered highly reliable, however come at a high price tag starting at 2500 Euros for the base model
3. Creality Ender 3 - Cheapest available chinese printer (about 200 Euros) that is also open source. Provides great quality prints and is also certified by OSHWA to be fully open source. However, only this model from Crealty.

---

There are many cheap 3D printer kits that are available these days like the Creality or Tronxy models. These work and do the job but the parts are not optimum or quality components. More often than not users upgrade their cheap printers and end up spending twice as much in the end by replacing components. It is preferrable then to build your own printer from scratch where you get to decide what components you would like to have in the printer. Also if something breaks, you know exactly what to do or how to fix it.

## Choosing a 3D Printer for the OLSK

The aim of the Open Lab 3D printer is to develop a DIY 3D Printer design that is robust and reliable. It should be possible for anyone to build it, with the posssibility of modifying it according to their budget, requirements and available resources. 

Requirements:
- The Open lab 3D Printer must be a reliable machine with quality components that can be built from scratch with having to source as few components as possible. 
- The printer can be built locally without having to purchase an expensive Kit or fully assembled 3D Printer. 
- The printer should make it possible for users to choose their own components for key elements depending on local availability of components and budget.

With quality components and systems being more expensive, 3 designs categories are proposed depending on the user budget. These are namely a high end model, a mid-consumer model and a budget model.

Using **MoSCoW** method of prioritization to prioritize the design objectives.
- **Must have (M)** — these are critical and must be included into the product. If even one isn’t included, the release is considered a failure. These can be downgraded if there’s agreement among stakeholders.
- **Should have (S)** — these requirements are important but not crucial for the release. They’re the first level of “Nice to have”, and generally share the importance of MUST requirements, without being so time-sensitive.
- **Could have (C)** — these requirements are desirable but not necessary for the release. They’re usually low-cost enhancements to the product. Due to their lower importance, they’re the second level of “Nice to have” features.
- **Won’t have (W)** — these are considered to be the least-critical or even not aligned with the product strategy. They are to be definitely dropped or to be reconsidered for future releases.

### 3D Printer Design Goals:
1. Enclosed and Stiff construction
2. Reliable and repeatable print quality
3. As much as possible can be built or sourced locally
4. Configurable based on local resources
5. No reliance on any manufacturer or proprietary hardware

### Technical Specifications
- Build Volume :- 200x200x200 to 400x400x400 (Configurable?)
- Built in Enclosure
- Enclosed ventilated electronics chamber
- Automatic bed levelling feature with inductive sensor
- Heated Build Platform (> 60 deg C?)
- High Quality hot end (E3D V6 or better)
- Use a direct-drive extruder (to print flexible filaments) / Bowden Extruder
- Swappable Nozzle (0.25, 0.4, 0.6, 0.8)
- PLA, ABS and other std. materials
- Detect if filament finishes mid build or gets stuck
- Detect power loss and resume build
- Removable build plate (flex to remove part)
- HEPA Filter with Fan to remove fumes (for ABS printing)
- Emergency Stop Button
- 32 bit control capability
- Linear guides for fast accelerations
- Wifi Connectivity
- IP Cam to see build in real time

### Further Requirements (For Workshops)

1. Printable assembly and build instruction required.
2. SD card, USB drive compatibility and display required. 
3. Aluminium profile ends needs to be closed with protection pads.
4. All holes in profiles, enclosure plates or build plate needs to be drilled and deburred in advance
5. Linear bearings needed to lubricated beforehand (or avoided by buying quality bearing like Misumi)
6. Threaded inserts (Einpressmutter) needs to be inserted into printed parts before workshop and check possibility to replaced inserts with nuts
7. Firmware should already be uploaded to controller board with the right settings.
8. All cables must be extended to have the right length with insertable JST ends (Workshop will have no soldering activity)
9. No sharp corners and electrical components all enclosed.

### Design Categories

1. **Design 1 High End**: (Similar or cheaper than Ultimaker < 2500€ )
   - High end 3D printer with profesional grade print quality and speed
   - Durable and high end components
   - Latest features
2. **Design 2 - Mid Consumer** (similar in price to Prusa i3 < 800€ )
   - Higher end 3D printer with most key advanced features that could be in the price range of the prusa i3
3. **Design 3 - Affordable** (Similar in price to Ender 3 < 200€ )
   - 3D printer that is affordable in the range of the cheapest commercial good quality printer like the popular Creality Ender 3
   - This could include similar design considerations in terms of components and mechanics
   - Fully DIY so it can be built by anyone and almost anywhere in the world
   - Should have all the very basics to allow someone to 3D print good parts

## Current 3D Printer Design Log

### Frame
For the outer frame of the design, we start with the Hypercube Evoution design by thingyverse user Scott3D which has a boxed construction that has been tried and tested for rigidity and stiffness. The boxed frame is also easier to enclose which is necessary for printing ABS that requires a controlled temperature environment for good layer adhesion. The enclosed frame is also more importantly kids friendly. The frame squareness after building can be verified by measuring the opposite diagonals. If they are the same you know have a square structure.   

### Mechanics
The design uses the CoreXY mechanism whereby the motors for the X and Y axis are fixed to the frame, which makes the X carriage that carries only the print head lighter than a conventional Cartesian design which has to carry the X axis motor. This should allow higher accelerations and printing speed.

### **CAD 3D Model**
Scott3D the original creator of the Hypercube Evolution design has uploaded a parametrized and configurable CAD file in Autodesk Inventor. This needs some modifications since the parameters don't work correctly in the original file. This is a good starting point but later designs will be carried out in Fusion 360 or Freecad. For now a build volume of 300cm3 will be used to build the first iteration. 

Part of the design criteria is to fit the power supply and electronics under the base. This means the 3D printer is extended on the bottom by 100 mm but it has a clean look with increased safety by keeping electronics out of reach of (kids). Moreover, transport is easier and the printer can be easily placed on a desk. The printer size is increased on the top by 100mm to accomodate the moving cables of the print head during printing. The final frame size can be adjusted after the first prototype build, considering how space can be optimized within the printer.

---

### Linear Bearings on Smooth rods Comparison

According to this [Discussion](https://www.3d-druck-community.de/showthread.php?tid=11060) on difference between various linear bearings
1. Bronze bushing - Smooth bearings but could suffer from slip-stick effect if not properly aligned. They also need to be regularly lubricated.
2. Linear ball bearings LMXUU (with X for shaft diameter) - Less smoother than bronze bushings but also louder. They are more forgiving of misalignment. They are also less susceptible to wear.
3. Igus bearings (special polymer bushing) - similar to bronze bushings and are also susceptible to slip-stick effect.

--- 

### Belts and Pulleys

Most commonly used belts in 3D printers are the GT2 belts which are toothed rubber belts which can have different reinforcing fibes such as steel, fibreglass or just rubber. The GT2 belt is usually 6mm in width and accordingly the pulleys used are GT2 pulleys. The size of the pulleys are specified by the inner bore diameter, the number of teeth and if the pulley is a smooth idler, toothed idler or toothed pulley to connect to a motor for instance. The various forms can be seen below.

6mm wide rubber GT2 belts are used in the design. According to experience for users of CoreXY printers, here belts with steel reinforcement are to be avoided since these are not elastic enough and so can cause wobbling or misalignment of the idlers. Rather fibre reinforced rubber belts are to be used.

1. Toothed GT2 Pulley
2. Smooth Idler GT2 Pulley
3. Toothed Idler GT2 Pulley

### **Electronics**

1. Control Board - MKS Sbase V1.3 - 32 bit board
2. EndStops
   - Mechanical
   - Optical 
3. Heat Bed Options
   - Silicone Bed Heater
     -  220V mains heating
     -  24V heating
   - MK2A Aluminium Heated bed
   - Kapton heater
   - PCB Heat bed
4. Z-bed levelling sensor choices:
   - Induction Sensor
   - BL touch
   - IR sensor
   - Piezo Sensor

Most common Z-bed autolevelling sensors are reviewed and tested in the following [video](https://www.youtube.com/watch?v=il9bNWn66BY&t=8s), whereby the most simple and precise seem to be the PINDA 8mm inductive sensor from Prusa and the LJ12A3-4-Z/BX inductive sensor.

Inductive sensors are affordable and precise enough for our application and so we will use a 12mm LJ12A3-4-z/BX inductive sensor. This has a 4mm metal detection distance (for ferrous metal like iron) but for non ferrous metals like aluminium this distance can be halved. So the inductive sensor will have to be placed about 2-3mm offset from the nozzle head.

1. Rasberry Pi for Octoprint (WiFi printing)
2. Fans for cooling electronics 
   - 6020 Fans for electronics enclosure cooling
   - 4020 Duct fan for part cooling
   - 5015 Fan for Hotend cooling 

### 


### **Configuring the MKS Sbase V1.3**

- [Installing and Configuring SBASE controller](http://folgerforum.com/t/installing-and-configuring-sbase-controller-for-ft-5/82)

- [More Info on the MKS SBASE](https://3daddict.com/mks-sbase-32bit-mainboard-controller/)


### Direct Drive vs Bowden Drive

Most filament 3D printers use either direct or bowden extrusion. Both set-ups use an extruder to push filament through a heated nozzle, either directly or through a bowden tube. Though similar, these extruder formats have major differences. See links below for more information. 

![Direct vs Bowden](https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2018/10/29154851/direct-vs-bowden-extrusion-craftunique-181026.jpg)

[Direct Drive Extruder Selection Guide](https://3dprintbeginner.com/direct-drive-extruder-buyers-guide/)


## **Z-axis design for lifting Build Platform**

The current design uses 2 lead screws attached to Nema17 motors ot lift the build platform along with 4 linear rods to constrain movement on XY plane. Other possible design variations for the Z-axis are discussed below. Using two motors to lift the bed can cause problems with synchronization. One option would be to use one motor along with pulleys and a belt to run multiple screws. This solves the problem of syncing.

[Discussion 1](https://www.reddit.com/r/Reprap/comments/eds3im/hypercube_vs_hypercube_evo_single_z_motor/), [Discussion 2](https://reprap.org/forum/read.php?397,880969) on RepRap Wiki about the optimal number of lead screws, motors and guide rails combination to lift the build platform of a 330x330 cast aluminium build plate. Some of the possible designs are:
1. Original HEVO design by Scott3D of 2 lead screws + 4 12mm linear shafts(rods), run by 2 Nema Motors
2. 4 lead screws along with 4 linear shafts on four corners of build platform driven by 1 Nema17 motor and a belt and pulleys.
3. 3 lead screws + 2 linear shafts run by pulleys/belt and single Nema17 motor. 
4. 2 lead screws +  4 linear shafts driven by 1 Nema17 motor with belt and pulleys
   - 20 teeth pulley on motor and 40 teeth pulleys on lead screw
   - [Thingiverse Design from Gavitka](https://www.thingiverse.com/thing:2761562) and his [CAD model on Onshape](https://cad.onshape.com/documents/e7649e3888db4d61f17f5d40/w/3f454d3ecf79df8221cb6c13/e/d05210b36b3cba6cdb46aa0a)

5. Single stepper motor with 2 belts on either side to lift bed [design]/https://drmrehorst.blogspot.com/2017/07/ultra-megamax-dominator-z-axis-design_65.html)  

![Picture](https://1.bp.blogspot.com/-u9eRaYliMXk/WXNsRPfCFKI/AAAAAAAAGYQ/dfTh0YxCREUJ3nuMf4MkaKtRsai3jYhXwCLcBGAs/s640/Z%2Baxis%2Brev3%2Bclose.jpg)

### Calculating travel on Z - axis
Using 1.8 degree motor with T8x8 four start lead screws gives 8mm travel per rotation with 200 steps (1.8 degree motor) and 400 steps (0.9 degree motor). This is 0.04 mm and 0.02 mm respectively. 

---- 
### **All Metal Hotend vs PTFE Lined Hotend**

All metal:

- Works well for high (+250ºC) temperatures filaments like nylon or PC.
- No need to replace the PTFE liner (pretty obvious).
- Retraction performs worse.
- Plastic can get stuck to the inner walls. This can lead to clogging, more likely when changing from ABS to PLA (higher temp plastic to lower temp plastic).

PTFE liner hotend:

- Limited working temperature. Above 250 PTFE will start to degrade.
- PTFE tube needs to be replaced more or less often, depending on the use of the printer.
- Retraction performs better.
- Plastic is less likely to get stuck in inner wall (PTFE is very nonstick).
- When using PTFE liner, the plastic is melted very close to the nozzle. Unlike other techniques, in FFF/FDM 3D printing this is more desirable. E.g. to avoid 'heat creep', for a better flow control and more accurate output dimension.
[Source](https://3dprinting.stackexchange.com/questions/2621/what-are-the-advantages-and-disadvantages-of-an-all-metal-hot-end-compared-to-on)

### Comparison of three popular Nozzles

1. Slice Engineering's Mosquito - 165 Euros
   - This is a high end well engineered hotend with a copper heat sink and copper alloy heater block.
2. Triangle Lab's Dragon - 60 Euros
   - The dragon design takes the heater block design from the E3D V6 and the heat sink design from the mosquito and at a price of 60 Euros is the better option of the three. The copper heatsink allows the possibility of a less bulky cooling fan option.
3. E3D V6 (Original or Triangle Lab clone) - 15 Euros is the tried and tested E3D design. The heat sink here is however aluminium which has much worse conductivity than copper and so needs much more cooling in an enclosed heated chamber for instance.

[Comparison Video from User Nero3DP](https://www.youtube.com/watch?v=kTI86ZhNLTw&t=69s) who has had thousands of hours printing experience with the respective nozzles.

All three print relatively similarly good for the average user if they are correctly calibrated. However, they each have some advantages and disadvantages as listed above.

Best Budget Option is the E3D V4 from trianglelabs which prints perfecttly well but maintainence and nozzle changes are difficult. Plus the heat exchanger is big and cumbersome which needs much more cooling. The design is also heavier than the other two designs. The dragon hotend seems to hit the sweet spot. Both the dragon and E3D V6 will be tested.

### Heated Bed Choice

[Online Calculator](https://jscalc.io/calc/uS8JYjYISgIvzJ1x) to calculate time needed to heat an aluminium or glass build plate with heatbed platform of specific size and power.

Minimum heatbed power to heat the aluminium build plate to print PLA (70degC) is about 

- use 12AWG wire from PSU>Mosfet and 12AWG Silicon high temp wire from mosfet to heater


Bed heater driven using a Solid State Relay
You can use a solid state relay (SSR) to switch the bed heater by connecting its control terminals to the Duet bed heater terminals. Make sure that you get the wires to the + and - control terminals of the SSR the right way round. 

Caution: when using a high-powered bed heater, in the event that temperature control fails and the bed heater is turned on at full power for an extended period of time, you should either make sure that the bed heater and bed will not exceed a safe temperature, or else install a thermal cutout to disconnect the bed heater or its power supply before excessive temperatures are reached.

Mains voltage AC bed heater
Use a zero-crossing DC-AC SSR such as Crydom D2425, Kudom KSI240D25-L or Fotek SSR-25DA (note: many Fotek SSRs on sale are fakes, using triacs rated at lower current than the marked rating of the SSR). For 230V bed heaters, SSR-10DA may be sufficient. If your bed heater draws more than about 1/4 of its rated current then the SSR may need a heatsink.

Take appropriate safety precautions when using a mains voltage bed heater. In particular:

Connect metal parts of the printer to mains ground. This includes the printer frame, the bed plate if it is conductive (e.g. aluminium), and any other metal parts that the bed heater or SSR wiring might come into contact with if wires break.

Ensure that it is not possible for the user to touch the SSR terminals or any other exposed mains wiring, or for you to touch the mains wiring when you are working on the printer with power applied. If your SSR is not supplied with a clear plastic safety cover, buy one (for the Crydom SSR listed above, the part number is KS101).

If the bed is moving, use highly-flexible wire or cable with a sufficient voltage rating to connect the moving bed heater to the stationary wiring. Cable intended for use in multimeter tests leads is one possibility.

If the bed is moving, you must use strain relief at both ends of that cable, to reduce the risk of the cable fracturing with repeated movement.
If the bed is moving, use a cable chain or similar to make sure that the cable can't get chafed or trapped.

Provide a fuse for the bed heater circuit or the whole printer appropriate to the current draw and the current rating of the mains lead. One option is to use a panel mount IEC mains inlet connector with a switch, neon indicator and fuse built in. See http://www.thingiverse.com/thing:965396 for an example setup on a delta printer.
It is highly recommended that you power the printer via a Ground Fault Current Interruptor (GFCI) - more commonly called a RCD (Residual Current Device) in the UK - to protect against electric shock in the event of a fault.
If in doubt, consult a qualified electrician.

**Low voltage (12V or 24V) DC bed heater**

Use a low voltage drop DC/DC SSR such as the Auber Instruments MGR-1DD80D100 or Crydom DC100D40. The SSR may need a heatsink, depending on the current. Do not be tempted to use a cheap DC-DC SSR such as the SSR-40DD, which is basically useless for this application because of its high voltage drop.

 If your heater will reach dangerously high temperatures if it is left on at full power, you should always use a TCO to guard against electronics or firmware failure. The alternative is to choose the heater power carefully so that it is powerful enough to reach the temperatures you want quickly, but not so powerful as to reach a dangerously high temperature when left fully on for a long time.

### Choosing Power Supply

[Info](http://customize-3d.com/how-to-calculate-power-requirements-of-3d-printer-safeguard-controller.html)

Connected Heated bed with Mosfet and power supply ![Picture](http://customize-3d.com/images/EasyBlog/power/heatedbed_wiring.jpg)

The next most important rating for your 3D printer power supply is output current. As previously stated, this will be the limiting factor for your heated bed and the total number of hot ends you are operating. 

The easiest way to calculate how many amps you need is to look at the wattage of the supply, and the rating of your heated bed. Generally, your control board, one hot end, five motors, and a few other electronics (sensors and fans) can be said to use about 100 watts of power. Add in what your heated bed is rated for, and you’ll have a minimum required wattage for your power supply.

To get the amperage, simply divide by the output voltage, e.g. 360W / 12V = 30A.

Here are some examples:

- The standard minimum for 3D printers is usually 240 watts (12 volts @ 20 amps). That would be enough for a printer with a single hot end and a heated bed around 180 x 180 mm.
- With a 200 x 200 mm bed or a second hot end, you’d be better off with 300 watts (12V @ 25A).
With both a second hot end and a 200 x 200 mm heated bed, or single extruder with up to a 300 x 300 mm heated bed, 360 watts are standard (12V @ 30A).
- With anything larger than that, a power supply rated upwards of 400W would be recommended.
With a 24V PSU, the rules change slightly, as you don’t need quite as much current. Still, we would recommend sticking to the above standard for the best possible operation.

There are two main kinds of power supplies:

ATX PSUs are generally meant for PCs, and while they are manufactured to a higher standard than LED supplies, they require modification to be used with 3D printers. They are also usually more expensive, especially at higher ratings, but incorporate many more safety features, not least of which is closed AC wiring and (usually) a power switch.

LED PSUs are what you will usually find on a kit like the Anet A8 — that clunky silver box with the really dangerous wiring terminal. These supplies are generally pretty reliable, and they’re cheap and easy to wire. However, there’s no real standard for these supplies to be manufactured to. That means quality will vary a lot based on the seller and manufacturer. The single biggest issue with these supplies is the open AC screw terminals, which are a safety hazard if left uncovered. 

The best way to check an LED PSU is with a voltmeter or multimeter. First, wire up your supply to the appropriate power cord, making sure that it is not plugged in while you are handling the wires. When done, plug in your supply, and check the voltage across the output terminals. Be very careful not to touch or short the input AC power terminals. 

When checked with a voltmeter, the output should be nearly the same as the indicated output voltage. You may need to fiddle with the small potentiometer next to the screw terminals, as this will change the output regulation slightly. Having the voltage slightly higher will help with an underpowered heated bed, but be careful not to overpower your other components.

Once you know your supply works, wiring it into your printer should be relatively easy. This is especially true with screw terminals and the plug-and-play style setup adopted by most manufacturers. Again, be very sure that you never do any wiring while the supply or the printer is plugged in. Also, be very careful to not touch or short the AC terminals of your PSU while running tests.

![Source PSU](https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2018/10/31172554/the-screw-terminals-on-an-led-psu-emmett-grames-all3dp-181028.jpg)


If you accidentally touch both AC terminals, or if they short to each other, it’s very easy to electrocute yourself or burn things down. Therefore, to finish up we would recommend you avoid either of these instances by investing the time and filament printing a covering for any exposed wires and terminals. It’s also a good idea to add a switch and a fuse in the process. Example of covering the PSU terminals below.

![PSU covering](https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2018/10/31172601/a-simple-safe-psu-cover-and-switch-mount-punishedprops-thingiverse-181028.jpg)
https://all3dp.com/2/3d-printer-power-supply-how-to-choose-the-right-one/

### 3D printer Wiring

One of the most important things about your 3D Printer is the Wiring. If the wire gauge is too small you won’t get enough current to your parts and can even lead to busted cables. Too large of a gauge and you might have a problem fitting those crimped terminals into the connectors.

So, the first thing to look for in a wire type is a stranded wire. The reason being your printer will create thousands of cycled movements and if your wire is a solid it will be susceptible to break over the duration of repeated abuse. The stranded wire allows for the cable to flex and move without breaking over time.

The RepRap Wiki page recommends using 22AWG or 20AWG


[Instructions](http://www.hobbycncaustralia.com.au/Instructions/iI1powerchassis.htm) on setting up the power connector.



![Power Connector](http://www.hobbycncaustralia.com.au/images/Instructions/1ChassisSetup/10Powerchassis.jpg)

**Points to be noted**

The hot end and thermister wires are not polarity dependent so the + and - can be plugged in anyway.

From top left, the motors will be connected in the order of X, Y, Z followed by the extruder motor. For the Z axis a splitter will have to be used to join both the Z-axis motors to a single Z port.

#### **3D Printer Wiring Connectors**

**Dupont Connectors**

These connectors work great for RAMPS and Rumba type controller boards with open Pin Headers.

![Dupont Connectors](https://3daddict.com/wp-content/uploads/2017/07/71V1MPH28AL._SL1001_-600x600.jpg)

The JST connectors are what are used on the 32 bi boards such as the MKS, AZteeg etc.

![JST](https://3daddict.com/wp-content/uploads/2017/07/619OVDmXWAL._SL1001_-600x600.jpg)

[Source](https://3daddict.com/3d-printer-wiring/)


Wiring exmaple of the MKS Sbase V1.3 Board - ![Schematic](https://cdn.shopify.com/s/files/1/0221/4487/6644/files/Circuit_Diagram_21478c66-4ec3-47ab-99f7-fc39ac6f9e7e_2048x2048.jpg?v=1560332582)

- Great [instructable](http://www.hobbycncaustralia.com.au/Electronics/ElecKit43.htm
) on wiring up the various electronics:


**Inserting threaded inserts in Printed Parts**

![Inserting Inserts](https://ae01.alicdn.com/kf/Hecc63a66816f436892d69678aa8ab5913.jpg)



### Optional Features (Future Work) ###

1. Detect and pause print when filament runs out
   - 3D printed [design](https://www.thingiverse.com/thing:285504/files) with mechanical roller switch 
2. Pause print when enclosure door opens
3. Pause print when enclosure door is opened

## Configuring Marlin for MKS Sbase v1.3

The compiling of the Marlin configuration can be done using visual studio. The Platform.io extension in visual studio needs to be installed before that. Platform.io can be used to make changes to the configuration file and upload the compliled firmware/configuraton file to the 3D printer. The latest release of [Marlin]() can be downloaded from Marlin's Github repository. Extract the contents of the zip folder and place the entire folder in the VS workspace.

Open the **platformio.ini** file in the **marlin-2.0.x** folder and change the **'default_envs'** variable of the default atmel to LPC1768. This is the CPU on the MKS Sbase V1.3, and needs to be defined here. Save and exit the platformio.ini file. 
---
Next Open the file **Configuration.h** with the file path - Marlin-2.0.x > Marlin > Configuration.h.

Here the first thing to define is the motherboard used i.e. here after #define MOTHERBOARD enter **BOARD_MKS_SBASE** as seen in the code snippet below.

Next basic settings such as:
 - Type of electronics
 - Type of temperature sensor
 - Printer geometry
 - Endstop configuration
 - LCD controller
 - Extra features

 - Advanced settings can be found in Configuration_adv.h

// Choose the name from boards.h that matches your setup
#ifndef MOTHERBOARD
  #define **MOTHERBOARD BOARD_MKS_SBASE**
#endif
---

Next for an NPN NO inductive probe like the set Z_endstop



## Calibration & Fine Tuning Methods

[Printer Calibration cross](https://www.thingiverse.com/thing:2484766) from thingiverse.

[Thingiverse Post](https://reprap.org/forum/read.php?397,530210) of finetuning the CoreXY setup

[Instructable on good practices for building 3d printer](https://www.instructables.com/An-Almost-Reliable-High-Precision-3D-Printer-Son-o/)

A nice way of presenting bill of materials ![Picture BOM](https://cdn.shopify.com/s/files/1/0221/4487/6644/files/2040_Z_Axis.jpg?v=1575871738)
