## Open Source Machines

1. The **Index** pick and place Machine from Stephen Hawes for assembling circuit boards automatically
    - [Github](https://github.com/sphawes/index) repository with Freecad files as well
    - [BOM](https://docs.google.com/spreadsheets/d/1N7jMZ2upi8-9_jjJnl2xC9bYtZuE1wX5Qzoa-qs21eY/edit#gid=476120456)
    - [Youtube video](https://www.youtube.com/watch?v=GtZcnIEi710)

