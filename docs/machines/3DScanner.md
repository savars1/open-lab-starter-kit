# 3D Scanner

## Commercial 3D Scanner Options

1. [Shining 3D Einscan SE](https://www.amazon.de/gp/product/B07111K9NM/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=gouza3dworld-21&creative=6742&linkCode=as2&creativeASIN=B07111K9NM&linkId=f5ef062743278471949724d4ff8ae53e) is a 3D scanner (colour scanner) with white light technology - resolution 0.1 mm - smallest scan area 30 x 30 x 30 mm - largest scan area 200 x 200 mm (turntable scan) and 700 x 700 x 700 mm (free scan/tripod). It has a rotating turntable with a fixed scanner and costs 1053 Euros.

![Shining 3D pic](https://images-na.ssl-images-amazon.com/images/I/71usf%2BnNSXL._SL1500_.jpg)


2. [Revopoint 3D Scanner](https://www.kickstarter.com/projects/2125914059/revopoint-pop-high-precision-3d-scanner-for-3d-printing?fbclid=IwAR0JoQ9EG02awxJ5Er74mv3IOJLHvaMxr62QGr6IcqKBQXYcHD3_zsPeO6c) is a small and compact 3D scanner developed on Kickstarter and costs about 250 Euros.



## Open Source 3D Scanner Options
1.	OpenScan EU – German maker – kit costs 50Euros
   - Youtube Page with build and use instructions:- https://www.youtube.com/channel/UCG3IgwSIFFlc77Luf9VVlYw^
   - Main Website: https://en.openscan.eu/openscan
   - BOM, CAD and build Instructions available

2. [FabScan](https://hci.rwth-aachen.de/fabscan) from Fablab Aachen.
    - Detailed instructable to build a [FabScan](https://www.instructables.com/3D-Scanner-FabScan-Pi/)

   ![FabScan](https://hci.rwth-aachen.de/files/migrated/images/FabScanPiFreigestellt.jpg)


3.	3D Scanner Turntable from youtuber Eric Strebel using Agisoft Metashape (commercial Software)
   - [Youtube Video](https://www.youtube.com/watch?v=28vrZIj-hYQ) 

4.	DIY solution with phone camera and Open source software Meshroom and Meshlab.
   - Youtuber [Crosslink](https://www.youtube.com/watch?v=45D0pFdqVgw)
