# Home

**Note on Ballscrews from user CMM on stackexchange:**

- At the limits of the machine, precision is limited by the achievable positioning accuracy of the motors and the mechanical reflection of that precision into a linear position.

- With a belt drive, the mechanical precision for a stepper-motor system is the circumference of the drive pulley divided by the number of distinct step positions. A 1.5 degree stepper with 10::1 microstepping (assuming everything works perfectly) with a 1 cm pulley gives a maximum precision of 0.013 mm. The same stepper motor with a 5 turns/in ball screw has a maximum precision of 0.0021 mm i.e. the precision is better.

Other advantages may include:

- A stiffer drive system with a higher resonant frequency

- Ability to transmit more force to the mechanism

- More precise slow-motion controls

There are several disadvantages.

- The rotating mass is increased.

- Rotating ball screws have a maximum rotation rate depending on how the ends are supported. This limits the maximum movement speed.

- The most economical ball screw size (at McMaster-Carr) is the 5/8"-5 ball screw with appropriate ball nuts. This is fairly high mass. It is also stiffer than thinner ball screws, and will have a higher maximum spin rate for a given support system.

- For a particular linear speed, the motors must spin faster. Unless a more complicated, variable micro-stepping drive method is used, the I/O load on the drive firmware will be higher -- about 5 times higher in this example.

- Other advantages of ball screws are less relevant in a 3D printer application.

- Ball screws can generate and support higher forces

- Ball screws are not subject to the belt stretching and skipping a tooth

- Ball screws work well with human-controlled knobs and hand-wheels

- If this is a larger bed size than a typical printer, you will probably be printing larger objects. To keep the printing time reasonable, you may want to print faster, which means higher acceleration and higher extrusion rates.

For your particular application, you need to evaluate the tradeoffs. Either could be your answer. If the analysis is too complex, you could default to belt drive. You could put the money you would have spent on ball screws, ball nuts, and extra bearings into wider belts, higher torque motors with smaller step angles, and better (higher voltage, faster switching) motor drives.

- There are two types of ball screws- rolled and ground. The cheap chinese ones are the rolled ones and specs won't be as good as the ground screws. 
- 
[Information](https://www.machinedesign.com/mechanical-motion-systems/linear-motion/article/21828146/the-importance-of-ballscrew-end-fixity) on fixing methods of ball screw ends and their importance