
[![Gitter](https://badges.gitter.im/fab-city/open-lab-starter-kit.svg)](https://gitter.im/fab-city/open-lab-starter-kit?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

[[_TOC_]]

# What is an Open Lab?

The idea of an Open Lab comes from the [Fablab](https://www.fablabs.io/) which is an openly accessible fabrication workshop with various propriertery machinery such as 3D printers, laser cutters and CNC mills. These propriertary machines listed on the [Fablab Inventory List](https://docs.google.com/spreadsheets/u/0/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html) which is an exhaustive list of machinery that can be bought and installed to setup up a fablab locally. These proprietary machines are relatively expensive and propietary and shipping from the US to lesser developed countries can be prohibitively expensive. Lastly propiertary machines can never really be owned and users are locked within the company ecosystem. So an Open Lab is basically a fab lab with open source hardware machinery instead of proprietary machinery. 

# What is the Open Lab Starter Kit?

The Open Lab Starter Kit aims to create An Open Source Hardware Repository for low marginal cost replication of Open Labs. The Project successively open sources standard fab lab inventory machinery. The aim of the Open Lab Starter Kit is to create a set of machines that are fully open source and can be fabricated to a large extent locally. The Bill of Materials (BOM), CAD models and a detailed and intuitive build instruction will accompany each design. Open source also means that the community can collaborate while suggesting changes and improvements. 

# Open Lab Machines

The preliminary list of machines that will be built in the open lab are as follows:

1. Desktop 3D Printer – min (20x20x20 cm) – max (40x40x40 cm)
2. Desktop CNC Mill also Small PCB Mill - min (20x20x10 cm) – max (60x40x15 cm)
3. Large 3D Printer – Big FDM (Daniele) - Volume 1m3
4. Large Format CNC Router – Size: 2.5 x 1.25 m
5. Heavy Duty CNC Milling Machine
6. Laser Cutter – (Daniele)
7. Small Desktop laser cutter - 600 x 400 mm
8. Laser cutter L– 1000 x 700 mm
9. 3D Scanner
10. Vinyl Cutter – A4 format
---
_Optional or Future Work_
1.  (Plasma Cutter) / Waterjet Cutter / metal cutter
2.  Sewing Machine (For Textiles)
3.  CNC Lathe
4.  Pick and Place Machine
5.  SLS Printer (Plastic / Metal)
---


# Open Lab Starter Kit Work Status

- **Starter Kit Documentation Taxonomy?**
  - machines
  - mechanics
  - framing
  - materials
  - electronics
  - software


<br>
<br>

- **Fab City project current status**

    - when the project officially ends?
        - 31.12 of 2024
    - updates about timeline if any
        - starter kit deadline flexible, best before the 4 years
    - when the buildings of the Fab House will be delivered?
        - June/July
    - who are the current partners
        - some Partners are not in the payroll
        - German Makerspaces
        - other Fab cities
        - Fab Lab Networks
        - companies
    - there are other possible partners? 
       - yes, possibility to add them also for us
       - efficient partnering, or they can be slowing down
       - core team first
      -  we can also talk to partners directly
    - can we access Open Lab in HSU?
        - yes
    - are we together with the other Fab Labs in Hamburg?
      - if so who/where they are?
        - Yes, Fab City Hamburg, San Pauli, Wellcome Werkstatt, company run labs
      - could we eventually access?
        - yes
      - could they help/cooperate somehow?
        - yes
- **working together**
    - situation of Pieter and Daniele 
       - 3 hours for Pieter, takes weeks to reconsider job 
        - Daniele can work anytime I can
    - weekly meeting  ~ 1 hour
      - they are set Friday 16:00-17:00
    - public and open source work/result
       - yes
    - working methodology
        - based on decentralized version control (git+gitlab)
        - probably gitlab will be self-hosted
        - our documents are all in markdown + other open formats(HTML/Web/etc)
        - how the documents will be published
           - let's discuss this Robin can help? Moritz?
        - which license should we use for our documents
           - [Creative Commons 4.0 Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
           - We will use different licenses: 
             - Single repo multi license
             - Multi repo multi license
        - weekly tasks
- **next steps**
    - research phase
      - not much time for it, maybe we can start iterating with our current experience
    - immediate focuses
      - workshops are starting soon for Fab City! as many workshops as possible
      - feedback from workshop
      - reproduction of existing machines
      - we really need documents and the hardware!
      - workshop schedule?
        - first 3 workshops, as corona allows it!
    - Mohammed + Pieter formation(?), in context, concrete scenarios
      - Daniele will try to teach the basics to start
      - Daniele will recommend activities/lecures/links etc
    - starter Kit:          
        - do we know the the procurement rules? (Who will procure what and how?)
          - we can be creative, because it is an highly bureaucratic process
          - subcontracting for chunks of purchasing
        - global index
          - we should make a the table of content of the starter kit
        - definition
          - we should formalize what the Starter Kit is
          - what are the goals
          - intended use
          - etc
        Mohammed did started it, let's make it together:
        - state of the art
        - commercial machines reference/benchmark
        - existing approaches
        - possible timeline
        - required tools and software
        - Open Lab + HSU fabrication lab available ro start playing


The documentation on this repository is licensed under the terms of the open source license: Creative Commons Attribution-ShareAlike 4.0 International ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)).
